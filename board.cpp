#include "board.h"

/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)) return;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, X, Y);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}

/*
 * Sets the board state given an 8x8 char array where 'w' indicates a white
 * piece and 'b' indicates a black piece. Mainly for testing purposes.
 */
void Board::setBoard(char data[]) {
    taken.reset();
    black.reset();
    for (int i = 0; i < 64; i++) {
        if (data[i] == 'b') {
            taken.set(i);
            black.set(i);
        } if (data[i] == 'w') {
            taken.set(i);
        }
    }
}

int Board::simpleHeuristic(Side side)
{
    int temp = countBlack() - countWhite();
    if (side == BLACK)
    {
	    return temp;	
	}
	else
	{
	    return -temp;	
	}	
}

int Board::cornerModifier(Side side)
{
    int temp = 0;
    if(get(BLACK, 0, 0))
    {
	    temp += 3;
    }
    if(get(WHITE, 0, 0))
    {
	    temp -= 3;	
	}
	if(get(BLACK, 0, 7))
	{
	    temp += 3;	
	}
	if(get(WHITE, 0, 7))
	{
	    temp -= 3;	
	}
	if(get(BLACK, 0, 0))
	{
	    temp += 3;	
	}
	if(get(WHITE, 7, 7))
	{
	    temp -= 3;	
	}
	
	if(side == BLACK)
	{
	    return temp;	
	}
	else
	{
	    return -temp;	
	}
	
}

int Board::minimaxHeuristic(Side side, int d)
{
	
    if(d == 0)
    {
	    return simpleHeuristic(side);	
	}
	else
	{
	    Move *myMove = new Move(0, 0);
	    Move *best = new Move(0, 0);
	    int bestScore = -1000;
	    bool isMove = false;
	    Side otherSide;
		if(side == BLACK)
		{
			otherSide = WHITE;	
		}
		else
		{
			otherSide = BLACK;	
		}
	    //loop for all possible moves
	    for(int i = 0; i < 8; i++)
	    {
		    for(int j = 0; j < 8; j++)
		    {
			    myMove->x = i;
			    myMove->y = j;
			    if(checkMove(myMove, side))	
			    {
				    isMove = true;
				    Board *temp = copy();
				    temp->doMove(myMove, side);
				    int otherScore = temp->minimaxHeuristic(otherSide, d - 1);
				    if(-otherScore > bestScore)
				    {
						best->x = i;
						best->y = j;
					    bestScore = -otherScore;	
					}	
				}
			}	
		}
		//end for-loop
		if(!isMove)
		{
		    Board *temp = copy();
		    temp->doMove(NULL, side);
		    return temp->minimaxHeuristic(otherSide, d - 1);	
		}
		else
		{
		    return bestScore;	
		}
	}	
}
