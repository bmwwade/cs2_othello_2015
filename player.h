#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <cstdio>
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
	Side mySide;
	Side otherSide;
	
    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    
    Board *myBoard;
};

#endif
