#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
	mySide = side;
	if(mySide == WHITE)
	{
	    otherSide = BLACK;	
	}
	else
	{
	    otherSide = WHITE;	
	}
	myBoard = new Board();
    
}

/*
 * Destructor for the player.
 */
Player::~Player() {
	delete myBoard;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
	if(opponentsMove != NULL)
	{
		myBoard->doMove(opponentsMove, otherSide);
	}
    Move *myMove = new Move(0, 0);
    Move *bestMove = new Move(0, 0);
    int bestScore = -1000;
    bool didChange = false;
    for(int i = 0; i < 8; i++)
    {
	    for(int j = 0; j < 8; j++)
	    {
		    myMove->x = i;
		    myMove->y = j;
		    if(myBoard->checkMove(myMove, mySide))
		    {
			    Board *temp = myBoard->copy();
			    temp->doMove(myMove, mySide);
			    Move *otherMove = new Move(0,0);
			    Move *bestOtherMove = new Move(0,0);
			    int otherBest = -1000, ctr;
			    for(int k = 0; k < 8; k++)
			    {
				    for(int l = 0; l < 8; l++)
				    {
					    otherMove->x = k;
					    otherMove->y = l;
					    if(temp->checkMove(otherMove, otherSide))
						{
						    Board *temp2 = temp->copy();
						    temp2->doMove(otherMove, otherSide);
						    if(testingMinimax)
						    {
								ctr = temp2->simpleHeuristic(otherSide);
							}
							else
							{
							    ctr = temp2->cornerModifier(otherSide);	
							}
							if(ctr > otherBest)
							{
							    bestOtherMove->x = k;
							    bestOtherMove->y = l;
							    otherBest = ctr;	
							}
							
						}
						
					}
						
				}
				
				if(-otherBest > bestScore)
				{
					bestMove->x = i;
					bestMove->y = j;
					bestScore = -otherBest;
					didChange = true;	
				}
				
				int t;
				if(testingMinimax)
				{
				    t = temp->simpleHeuristic(mySide);	
				}
				else
				{
				    t = temp->cornerModifier(mySide);	
				}
				
				if(t > bestScore)
				{
				    bestMove->x = i;
				    bestMove->y = j;
				    bestScore = t;
				    didChange = true;				    	
				}
					
			}	
		}
	}
	//end outermost for-loop
	if(didChange)
	{
	    myBoard->doMove(bestMove, mySide);
	    return bestMove;	
	}
	else
	{
	    return NULL;	
	}
    
}
